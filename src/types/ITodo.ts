export interface IState {
  todos: ITodo[];
  activeFilter: ClassFilterType;
}

export type ClassFilterType = 'All' | 'Active' | 'Done';

export interface IFormState {
  isFormVisible: boolean;
  todoText: string;
}

export interface IStats {
  active: number;
  done: number;
}

export interface IFilterState {
  filters: ClassFilterType[];
}

export interface ITodo {
  id: number;
  text: string;
  completed: boolean;
}
